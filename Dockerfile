# Pull base image
FROM gettyimages/spark

ENV SPARKMASTER localhost:7077
ENV ZK localhost:2181
ENV GROUP sparkgroup
ENV TOPICS mytopic
ENV JAR /tmp/SparkLetterCount.jar

# Run it
CMD $SPARK_HOME/bin/spark-submit --class "SparkLetterCount" --master spark://$SPARKMASTER $JAR $ZK $GROUP $TOPICS 1
