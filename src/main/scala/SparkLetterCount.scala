// scalastyle:off println

import java.util.HashMap

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._

object SparkLetterCount {
  def main(args: Array[String]) {
    if (args.length < 4) {
      System.err.println("Usage: SparkLetterCount <zkQuorum> <group> <topics> <numThreads>")
      System.exit(1)
    }

    val Array(zkQuorum, group, topics, numThreads) = args
    val sparkConf = new SparkConf().setAppName("SparkLetterCount")
    val ssc = new StreamingContext(sparkConf, Seconds(2))
    ssc.checkpoint("checkpoint")

    val topicMap = topics.split(",").map((_, numThreads.toInt)).toMap
    val lines = KafkaUtils.createStream(ssc, zkQuorum, group, topicMap).map(_._2)
    val letters = lines.flatMap(_.toLowerCase)
    val letterCounts = letters.countByValueAndWindow(Seconds(10), Seconds(4), 2)
    println("Here we go...")
    letterCounts.print()
    println("...Done")

    ssc.start()
    ssc.awaitTerminationOrTimeout(1000*180)
    ssc.stop()
  }
}

// scalastyle:on println
